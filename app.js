const express = require('express')
const app = express()
const port = 2563

app.get('/', (req, res) => {
  res.send('DEMO GITLAB CICD ')
})

app.listen(port, () => {
  console.log(`Application is listening at http://localhost:${port}`)
})
